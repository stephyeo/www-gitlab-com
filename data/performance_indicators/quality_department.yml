- name: Quality Hiring Actual vs Plan
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  definition: Employees are in the division "Engineering" and department is "Quality".
  target: 24 by November 1, 2019
  org: Quality Department
  health:
    level: 3
    reasons:
    - Engineering is on plan. But we are lending some of our recruiters to sales for
      this quarter. And we just put in place a new "one star minimum" rule that might
      decrease offer volume.
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8610186
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Quality Department
  is_key: true
  health:
    level: 0
    reasons:
    - Currently finance tells me when there is a problem, I’m not self-service.
    - Get the budget captured in a system
    - Chart budget vs. actual over time in periscope
  urls:
  - https://app.periscopedata.com/app/gitlab/633240/Quality-Non-Headcount-BvAs
- name: Quality Department New Hire Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've fluxuated above and below the target line recently, which for a small
      department is not worrisome
  sisense_data:
    chart: 9389208
    dashboard: 719540
    embed: v2
- name: Quality Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are at our target of 0.58 exactly overall, but trending upward.
    - We need to get the target location factors in the charts.
    - It will probably be cleaner if we get this in periscope.
    - We need to set the target location factors on our vacancies and make sure recruiting
      is targeting the right areas of the globe on a per role basis.
  sisense_data:
    chart: 7045895
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality Handbook Update Frequency
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching <code>/source/handbook/engineering/quality/**</code>over
    time.
  target: Above 35
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Increased from 22 to 31 in August. Still under target of 35
    - We are making progress but still under target, continuing to champion small
      iterations and being handbook first
  sisense_data:
    chart: 8073930
    dashboard: 621059
    shared_dashboard: 3f0e7d87-fc13-44c3-bbba-1a9feb48c3bd
    embed: v2
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7195
- name: Quality Discretionary Bonus Rate
  base_path: "/handbook/engineering/quality/performance-indicators/index.html#quality-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Quality department
    every month.
  target: At 10%
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Started tracking per deparment, set to problem as we have been at 0% for the last two months.
    - We communicated changes on requiring more detailed information on submitting discretionary bonuses, this may have had an adverse reaction. We plan to champion and highlight achievements more broadly.
  sisense_data:
    chart: 9524996
    dashboard: 516343
    embed: v2
- name: Review App deployment success rate for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of our test tooling to enable engineering efficiency.
  target: Above 99%
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Improvements in review app improvements and stability have been deprioritized in favor of Engineering Metrics and Community Contribution work streams. 
    - Success rate remains above 90% for second consecutive month
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/605
  - https://gitlab.com/groups/gitlab-org/-/epics/606
  sisense_data:
    chart: 6721558
    dashboard: 516343
    embed: v2
- name: GitLab project master pipeline success rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of the GitLab project <code>master</code> pipelines
    to accelerate cycle time of merge requests, continuous deployments and GitLab
    EE releases.
  target: Above 95%
  org: Quality Department
  is_key: true
  health:
    level: 3
    reasons:
    - Increased health from attention to okay (green-3) due to meeting the target from defensive changes and increased participation from development organization
    - Merge train was piloted for `gitlab-org/gitlab` for 8 hours and identified issues to move forward
    - Inadequate personnel coverage during AMER working hours has lead to longer resolution time. Future team additions in AMER timezone will help. 
    - +50% of master-broken issues occurs during AMER hours and average MTTR is 20% higher than other failures.
  urls:
  - https://app.periscopedata.com/app/gitlab/564156/Engineering-Productivity---Pipeline?widget=8324723&udv=895971
  - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/195#known-issues-improvements
  sisense_data:
    chart: 8573283
    dashboard: 516343
    embed: v2
    daterange:
      days: 730
- name: Average merge request pipeline duration for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average duration of our merge request pipelines to accelerate
    our development cycle time, and continuous deployments. See epics <a href="https://gitlab.com/groups/gitlab-org/-/epics/1853">1853</a>
    and <a href="https://gitlab.com/groups/gitlab-org/-/epics/25">25</a> for individual
    work items.
  target: Below 30 minutes
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - More longer frontend and qa pipelines running compared to backend/docs
    - Increase average duration for most pipeline types due to 2 minute average increase in bottlenecked jobs and addition of rspec:coverage and frontend coverage jobs to dogfood test coverage functionality
    - Review app deployment adds 8-10 minutes per frontend pipeline and make up 29% of pipelines
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/237841
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7189
  - https://gitlab.com/groups/gitlab-org/-/epics/1853
  - https://gitlab.com/gitlab-org/gitlab/-/issues/254233
  sisense_data:
    chart: 6782964
    dashboard: 516343
    embed: v2
- name: Quality Department Narrow MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Quality Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is a key indicator showing how productive our team members are based
    on the average MR merged per team member. We currently count all members of the
    Quality Department (Director, EMs, ICs) in the denominator because this is a team
    effort. The <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv">projects
    included</a> contributes to the overall product development efforts.
  target: Above 10 MRs per Month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Upgraded to attention since we hit our target of 10 in July 2020
    - Working on increasing Quality codebase maintainers and champion smaller iterations
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/534
  sisense_data:
    chart: 8467527
    dashboard: 654023
    shared_dashboard: 4278f770-7709-4a5e-89f7-812a319c2fbb
    embed: v2
- name: Average duration of end-to-end test suite execution on CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average duration of our full QA/end-to-end test suite in
    the <code>master</code> branch to accelerate cycle time of merge requests, and
    continuous deployments.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven't started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/198
- name: Ratio of quarantine vs total end-to-end tests in CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability and effectiveness of our QA/end-to-end tests
    running in the <code>master</code> branch.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/199
- name: Average cost per merge request pipeline for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the cost per pipeline to measure engineering efficiency. This
    is calculated by taking the total cost of all merge request pipelines divided
    by the number of pipelines.
  target: Below "$1.25"
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Cost per pipeline has increased since 2020-08-01 due to new jobs added to support Dogfooding, increase in build time for jobs that have multiple builds per pipeline, and overall increased duration. Engineering Productivity is focused on reducing cost with <a href="https://gitlab.com/groups/gitlab-org/-/epics/3806">dynamic test selection</a>.
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8611
  - https://gitlab.com/groups/gitlab-org/-/epics/3806
  sisense_data:
    chart: 8346949
    dashboard: 516343
    embed: v2
- name: New issue first triage SLO
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure our speed to triage new issues. We currently have ~400 new issues
    every week in CE/EE. We need to go through all of them and identify valid issues
    and high severity bugs.
  target: Below 5 days
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet. We have made progress on fanning out first
      triage to Engineers in the Quality Department.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
    - Fan out triaging to all of Engineering and not just the Quality Department.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/136
  - https://gitlab.com/groups/gitlab-data/-/epics/50
- name: Monthly new bugs per stage group
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Tells us the hit rate of defects per each stage group on a monthly basis
  target:
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Defect trends on a high level is increasing for mature areas of the product,
      but also due to double counting from duplicate issues. We need to raise more
      awareness and encourage teams to look at this metric and evaluate themselves
      regularly.
    - Add this metric into every stage groups' dashboard to raise awareness.
    - Define threshold and migrate into Periscope. https://gitlab.com/gitlab-data/analytics/issues/1792
    - Define a mechanism to filter duplicate issues.
  urls:
  - https://quality-dashboard.gitlap.com/groups/gitlab-org/bugs_by_team
- name: Open and close severity::1-severity::2 product defects per month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Tells us the monthly creation and resolution of high severity severity::1 and
    severity::2 bugs aggregated from each product group.
  target:
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We have started to measure this, need to make this a PI
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7237
  - https://about.gitlab.com/handbook/engineering/quality/quality-bugs-open-vs-closed-rate/
- name: Mean time to resolve severity::1-severity::2 bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Tells us the monthly average time to resolve high severity defects.
  target:
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - We have prioritized efforts and trend is stabilizing. We have equiped engineering
      groups with better weeky triage reports. We will likely not trend downwards
      until the backlog of older bugs are closed.
    - Migrate to GitLab Insights dashboard https://gitlab.com/groups/gitlab-org/-/insights.
    - Add integration into Periscope as a GitLab feature.
    - Add this metric into every stage groups' dashboard to raise awareness.
    - Define a mechanism to filter duplicate issues.
  urls:
  - https://quality-dashboard.gitlap.com/groups/gitlab-org/time_to_close_bugs
- name: severity::1 open bugs past target SLO
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of severity 1 bugs that have been open longer than 30 days
  target: TBD
  org: Quality Department
  is_key: true
  health:
    level: 1
    reasons:
    - Target to be determined, we recently adjust SLO to base on severity. Working on providing bug SLO to all product groups.
    - This is a lagging indicator, corrective actions done in the current month will not show PI improvement based on the SLO
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/3580
  sisense_data:
    chart: 9464298
    dashboard: 576726
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: severity::2 open bugs past target SLO
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the number of bugs past the priority SLO timeline.
  target: Target to be determined, we recently adjust SLO to base on severity. Working on providing bug SLO to all product groups.
  org: Quality Department
  is_key: true
  health:
    level: 1
    reasons:
    - Target to be determined, we recently adjust SLO to base on severity. Working on providing bug SLO to all product groups.
    - This is a lagging indicator, corrective actions done in the current month will not show PI improvement based on the SLO
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/3580
  sisense_data:
    chart: 9464306
    dashboard: 576726
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Ratio of bugs triaged with Severity (and priority)
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure our ability to differentiate high severity defects from the
    pool so we can prioritize fixing them above trivial bugs.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - The number of untriaged bugs have been stable but we can still do better. We
      need to establish an actionable threshold for each team.
    - Convert chart to time series data and define threshold.
    - Migrate into Native GitLab Insights dashboard https://gitlab.com/groups/gitlab-org/-/insights.
    - Add integration into Periscope as a GitLab feature.
    - Providing a weekly bug report with a distribution matrix of priority and severity.
      - https://gitlab.com/gitlab-org/quality/triage-ops/issues/186
    - Provide training for product/engineering groups on how to effectively use Priority
      and Severity. - https://gitlab.com/gitlab-org/quality/team-tasks/issues/148
    - Enforce triaging escalations (based on stage group labels) for bugs with no
      severity/priority labels.
  urls:
  - https://quality-dashboard.gitlap.com/groups/gitlab-org/bug_classification
- name: The ratio of closed (not merged MRs) vs merged MRs over time.
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure amount of throw away work vs merged.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - From experimental charts we don’t have that much throw away work. Not giving
      a passing grade because we don’t have a threshold to grade against.
    - Define threshold.
    - Migrate into Native GitLab Insights dashboard https://gitlab.com/groups/gitlab-org/-/insights.
    - Add integration into Periscope as a GitLab feature.
    - Analyze throw away work and root cause the reason. Improve planning and collaboration.
  urls:
  - https://quality-dashboard.gitlap.com/groups/gitlab-org/mrs_by_state
- name: Software Engineer in Test Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Software Engineers in Test against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 42 Software Engineers in Test
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 38% of target
  sisense_data:
    chart: 9668867
    dashboard: 516343
    embed: v2
- name: Engineering Productivity Engineer Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Engineering Productivity Engineers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 12 Engineering Productivity Engineers
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 33% of target
  sisense_data:
    chart: 9669589
    dashboard: 516343
    embed: v2
- name: Quality Engineering Manager Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Quality Engineering Managers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 5 Quality Engineering Managers
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently at 80% of target
  sisense_data:
    chart: 9669154
    dashboard: 516343
    embed: v2
- name: Unique Community Contributors per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Distribution of unique authors for all merged Community contribution MRs by month.
  target: Above 150 contributors per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9522755
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Community MR Coaches per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: The number of MR Coaches defined by team.yml role
  target: Above 50 coaches per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9721107
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Community Contribution Mean Time to Merge
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average days from creation to merge for Community contribution MRs by month
  target: Below 10 days
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9555034
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Percent of Feature Community Contribution MRs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percentage of merged `~"Community contribution"` MRs that are labelled `~feature`
  target: Above 30%
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9640193
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365

