---
layout: handbook-page-toc
title: "Risk and Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Risk and Field Security Mission
The Risk and Field Security team serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers. We work with all GitLab departments to document requests, analyze the risks associated with those requests, and providing value-added remediation recommendations.

<!-- Hold for future use ## Roadmap
Field Security does not yet have a roadmap but is planning to have one completed by the end of 2020--->

### Risk and Field Security Primary Functions
The Risk and Field Security currently focuses on the following Risk Categories:

_Field Security_: In support of our Sales Team, the Risk and Field Security team responds to [Customer Security Assessments](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) and maintains the [Customer Assurance Package](/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html). We also provide training on [Security Best Practices](/handbook/security/) both inside and outside of GitLab. 

_Third Party Risk Management_: Whenever a Third Party is introduced into the GitLab environment, there is a risk that their poor security posture can negativly impact GitLab. In order to reduce this risk, the Risk and Field Security team conducts [Vendor Risk Assessments](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html)- a process that can help identify potential security risks associated with a third party. 

_Security Operational Risk Management_: Focused on Tier 2/Operational Risks, we conduct [regular and ad-hoc risk assessment activities](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/risk-management.html) to identify opportunities for risk reduction. 

### Makeup of the team
- @dsharris - https://about.gitlab.com/company/team/#dsharris
- @jblanco2 - https://about.gitlab.com/company/team/#jblanco2
- @mmaneval20 - Manager- Risk and Field Security - https://about.gitlab.com/company/team/?department=security-department#mmaneval20
- @julia.lake - Director of Risk and Compliance - https://about.gitlab.com/company/team/#julia.lake

## Contact the Field Security Team
* Email
   * `fieldsecurity@gitlab.com`
* Slack
   * Feel free to tag is with `@field-security`
   * The `#sec-fieldsecurity`, `#sec-assurance`, `#security-department` slack channels are the best place for questions relating to our team (please add the above tag)
* [GitLab field security project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/field-security)
