---
layout: markdown_page
title: "Category Direction - Jupyter Notebooks"
description: "With GitLab you can store and version control Jupyter notebooks in the same way you store packages and application code. Learn more!"
canonical_path: "/direction/package/jupyter_notebooks/"
---

- TOC
{:toc}

## Jupyter Notebooks

Jupyter Notebooks are a common type of code used for data-science use cases. With GitLab you can store and version control those notebooks in the same way you store packages and application code.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=jupyter)
- [Overall Vision](/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)
- [Documentation](https://docs.gitlab.com/ee/user/project/repository/#jupyter-notebook-files)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com)).

## Use cases

1. Use multiple language kernels to explore and visualize data.
1. Work collaboratively and securly by checking your notebook into version control.
1. Create and share environments for hosting notebooks.
1. Share notebooks and code snippets with team members.
1. Create runbooks using nbconvert for code execution and workflow simplification.

## What's next & why

Our next set of priorities for Jupyter Notebooks, are about resolving issues with how notebooks are rendered within Gitlab. Since Jupyter is often used to present data and findings, it's important that we render images and content properly. [giltab-#18540](https://gitlab.com/gitlab-org/gitlab/issues/18540) will resolve an issue where images in markdown cells are not displayed. 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- Resolve issues with how Jupyter Notebooks are rendered within GitLab:
  - [Images in markdown cells are not displayed](https://gitlab.com/gitlab-org/gitlab/issues/18540)
  - [SVG images are not displayed in Jupyter / IPython Notebooks](https://gitlab.com/gitlab-org/gitlab/issues/22519)
  - [HTML within Markdown in Jupyter notebooks not being rendered](https://gitlab.com/gitlab-org/gitlab/issues/27455)
  - [Render LaTeX and images, correctly in Jupyter notebooks](https://gitlab.com/gitlab-org/gitlab/issues/28601)
  - [Render single escaped carriage return or newline in code block within a notebook markdown cell](https://gitlab.com/gitlab-org/gitlab/issues/26143)
  - [Allow local linking to # chapter](https://gitlab.com/gitlab-org/gitlab/issues/22333)


## Competitive landscape

* [Domino Data Lab](https://www.dominodatalab.com/)
* [Dataiku](https://www.dataiku.com/)
* [Rapid Miner](https://rapidminer.com/) 

​​
## Top Customer Success/Sales issue(s)

The top customer success / sales issue is [gitlab-#33193](https://gitlab.com/gitlab-org/gitlab/issues/33193), which will improve the Jupyter installation process by giving improved error / success messages. 

## Top user issue(s)

The top user issue is [gitlab-18540](https://gitlab.com/gitlab-org/gitlab/issues/18540), which details a problem with image rendering within Jupyter notebooks. 

## Top internal customer issue(s)

The top internal customer issue is [gitlab-#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427), which will empower the [Release team](https://about.gitlab.com/handbook/engineering/development/ci-cd/release/), to utilize Jupyter Notebooks as a runbook to create a checklist/documentation for releases. 

## Top Vision Item(s)

The top vision item is [gitlab-#22723](https://gitlab.com/gitlab-org/gitlab/issues/22723), which will add support for BinderHub, a dependency manager for Jupyter Notebooks. This will allow users to build notebooks on-demand and provide customized computing environments, built from their Git repo.

